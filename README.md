## styles
### tkmiz
embedding, trained on NAI, 16 vectors per word, .005 learning rate, 35000 steps  
dataset: 179 items, captions and images downloaded from booru
![](sample_images/tkmiz.png)

### mhug
embedding, trained on NAI, 16 vectors per word, .005 learning rate, 35000 steps  
dataset: 260 items, images from twitter, captioned by deepdanbooru
![](sample_images/mhug.jpg)

### 1nupool
embedding, trained on NAI, 16 vectors per word, .005 learning rate, 35000 steps  
dataset: 74 items, images from danbooru
![](sample_images/1nupool.png)

### minimal_1
hypernetwork, trained on NAI, 0.000005 learning rate, 35000 steps  
dataset: 1224 items, images from twitter, captioned by deepdanbooru
![](sample_images/minimal_1.jpg)